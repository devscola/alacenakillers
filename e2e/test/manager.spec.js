let alertStub;
const HOME_ROUTE = '/'

describe('Alacena', () => {
  beforeEach(() => {
    cy.visit(HOME_ROUTE)
    waitForComponentsToMount()
  });

  it('shows an error if trying to add an empty product', () => {
    listenForAlerts();

    const alertText =
      'Campo Requerido Ausente. \n      No puedes añadir un producto sin nombre';

    getComponent()
      .shadowFind('component-button')
      .shadowFind('button')
      .shadowClick()
      .then(() => expectError(alertText));
  });

  it('can add products to the shopping list', () => {
    const aProduct = 'cacahuetes'
    const aQuantity = '3'
    const BORRAR = '{del}'

    getComponent().shadowFind('text-input').shadowFind('input').shadowType(aProduct)
    getComponent().shadowFind('number-input').shadowFind('input').shadowType(BORRAR).shadowType(aQuantity)
    getComponent().shadowFind('component-button').shadowFind('button').shadowClick()

    getComponent().shadowFind('component-list')
      .shadowFind('component-li')
      .shadowContains(aProduct)
      .shadowContains(aQuantity)
  });

  it('can delete a product from the list', () => {
    const aProduct = ['cacahuetes', 'altramuces', 'patatas']
    const aQuantity = '2'
    const BORRAR = '{del}'

    aProduct.forEach(element => {
      getComponent().shadowFind('text-input').shadowFind('input').shadowType(element)
      getComponent().shadowFind('number-input').shadowFind('input').shadowType(BORRAR).shadowType(aQuantity)
      getComponent().shadowFind('component-button').shadowFind('button').shadowClick()
      cy.wait(1000)
    });

    getComponent().shadowFind('component-list')
      .shadowFind('component-li')
      .shadowContains('altramuces')
      .shadowFind('.deleteButton')
      .shadowFind('button').shadowClick()

    cy.wait(1000)

    getComponent().shadowFind('component-list')
      .shadowFind('component-li')
      .its('length')
      .should('eq', 2)
  })

  it('persist the list after refresh the web page', () => {
    const aProduct = ['cacahuetes', 'aceitunas', 'patatas']
    const aQuantity = '2'
    const BORRAR = '{del}'

    aProduct.forEach(element => {
      getComponent().shadowFind('text-input').shadowFind('input').shadowType(element)
      getComponent().shadowFind('number-input').shadowFind('input').shadowType(BORRAR).shadowType(aQuantity)
      getComponent().shadowFind('component-button').shadowFind('button').shadowClick()
      cy.wait(1000)
    });

    cy.reload(true)

    cy.wait(1000)

    getComponent().shadowFind('component-list')
      .shadowFind('component-li')
      .its('length')
      .should('eq', 3)
  });

  it('does not can click the list buttons in edit mode', () => {
    const aProduct = ['cacahuetes', 'pipas', 'patatas']
    const aQuantity = '2'
    const BORRAR = '{del}'

    aProduct.forEach(element => {
      getComponent().shadowFind('text-input').shadowFind('input').shadowType(element);
      getComponent().shadowFind('number-input').shadowFind('input').shadowType(BORRAR).shadowType(aQuantity);
      getComponent().shadowFind('component-button').shadowFind('button').shadowClick()
      cy.wait(1000)
    });

    cy.wait(1000)

    getComponent().shadowFind('component-list')
      .shadowFind('component-li')
      .shadowContains('pipas')
      .shadowFind('#modifyButton')
      .shadowFind('button').shadowClick()

    cy.wait(1000)

    getComponent().shadowFind('number-input').shadowFind('input').shadowType(BORRAR).shadowType('22');
    getComponent().shadowFind('component-button').shadowFind('button').shadowClick()
  });
});

const getComponent = () => {
  return cy.get('#main').find('manager-component')
}

const waitForComponentsToMount = () => {
  cy.wait(200);
};

const listenForAlerts = () => {
  alertStub = cy.stub();
  cy.on('window:alert', alertStub);
};

const expectError = (message) => {
  expect(alertStub.getCall(0)).to.be.calledWith(message);
};
