# ALACENA KILLERS by DEVSCOLA

This application is make to learn Javascript

## Table of Contents

- [ALACENA KILLERS by DEVSCOLA](#alacena-killers-by-devscola)
  - [Table of Contents](#table-of-contents)
  - [System requeriments](#system-requeriments)
  - [Environment variables](#environment-variables)
  - [How to run the application](#how-to-run-the-application)
  - [How to run Unitary tests](#how-to-run-unitary-tests)
    - [E2E tests in console](#e2e-tests-in-console)
    - [E2E tests in browser](#e2e-tests-in-browser)
  - [How to open cypress in a browser](#how-to-open-cypress-in-a-browser)
  - [How to open cypress in console](#how-to-open-cypress-in-console)
  - [Deploy demo](#deploy-demo)

## System requeriments

- Recommend only use the application in Chrome browser.
- Chrome last version  79.0.3945.88.
- Docker-compose version 1.24.1.

## Environment variables

- For use this var make file `.env` in root: `API_URL = http://localhost:8081`.
- For deploy in Heroku we use :
	- `APP_DIR: $CI_PROJECT_DIR/app/`.
  	- `API_DIR: $CI_PROJECT_DIR/api/`.

## How to run the application

- Run: `docker-compose up --build api app`.
- Open in browser: 
  - APP: `localhost:8080`.
  - API: `localhost:8081`.

## How to run  Unitary tests

- Unitary test if docker-compose is up: `docker-compose exec app npm run test`.
- Unitary test if docker-compose is down: `docker-compose run --rm app npm run test`.
- Do the same for Api test.

## How to run Acceptance test

  ### E2E tests in console
    - Run: 
      `cd e2e
      docker run -v $(pwd):/app -w /app node:alpine npm install
      cd ..
      docker run --network="host" -v $(pwd)/e2e:/workdir rdelafuente/cypress
      `.

  ### E2E tests in browser
    - Run:
      - `cd e2e`
      - `npm install` (it will take a whilte)
      - `npm run cypress:open`

  ### How to open cypress in a browser
    - run-browser-cypress-test.sh

  ### How to open cypress in console
    - ./run-console-cypress-test.sh



## Deploy demo

Currently, we have a  deploy  application for demo purposes, you can check it out at:

- Heroku Cloud Application Plataform:

  - [APP](https://alacenakillers-app.herokuapp.com "Complete Aplication").
  - [API](https://alacenakillers-api.herokuapp.com "Only Api").

![Heroku: Cloud Application Platform](https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2016/04/1461122387heroku-logo.jpg)

