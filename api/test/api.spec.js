const request = require('supertest')
const api = require('../src/Api')

describe('Api', ()=>{
  it('Is listening', (done)=>{
    request(api)
      .get('/')
      .set('Accept', 'text/html')
      .expect('Content-Type', /text/)
      .expect(200, done)
  })

  it('CreateUser endpoint creates users', (done)=> {
    const expected = 0
    request(api)
      .get('/createUser')
      .then((res)=>{
        expect(res.body).toEqual(expected)
        done()  
      })
  })

  it('Create endpoint returns the sent product',(done)=>{
    const productSent = {'product':'Patatas 1', 'userId':0}
    const expected = 'Patatas 1'
    
    request(api)
      .post('/create')
      .set('Content-Type', 'application/json')
      .send(productSent)
      .then((res)=>{
        expect(res.body).toEqual(expected)
        done()  
      })
  })

  it('Retrieve endpoint return all products',(done)=>{
    const firstProduct = {'product':'Pepinos 1', 'userId':0}
    const expected = ['Patatas 1', 'Pepinos 1']
    
    request(api)
      .post('/create')
      .set('Content-Type', 'application/json')
      .send(firstProduct)
      .end(()=> {})
          
    request(api)
      .get('/retrieveProducts')
      .then((res)=>{
        expect(res.body).toContain(expected[0])
        expect(res.body).toContain(expected[1])
        done()  
      })
  })

  it('Delete endpoint removes product',(done)=>{
    const expected = ['Patatas 1']
    
    request(api)
      .post('/deleteProduct')
      .set('Content-Type', 'application/json')
      .send({'productPosition':1, 'userId':0})
      .then((res)=>{
        expect(res.body).toContain(expected[0])
        done()  
      })
  })

  it('RetrieveUserUniqueProduct endpoint retrieves product and quantity of specific user', (done)=> {
    const firstProduct = {'product':'cacahuetes 1', 'userId':0}
    const secondProduct = {'product':'ciruelas 1', 'userId':0}
    const expected = 'cacahuetes,1'

    request(api)
      .post('/create')
      .set('Content-Type', 'application/json')
      .send(firstProduct)
      .end(()=> {})
  
    request(api)
      .post('/create')
      .set('Content-Type', 'application/json')
      .send(secondProduct)
      .end(()=> {})

    request(api)
      .post('/retrieveUserUniqueProduct')
      .set('Content-Type', 'application/json')
      .send({'productPosition':1, 'userId':0})
      .then((res)=>{
        expect(res.body).toContain(expected)
        done()  
      })
  })

  it('modifyProduct endpoint modify product and quantity of specific user', (done)=> {
    const modifyproduct = 'pepinos 2'
    const expected = ['Patatas 1', 'pepinos 2', 'ciruelas 1']

    request(api)
      .post('/modifyProduct')
      .set('Content-Type', 'application/json')
      .send({'product':modifyproduct, 'position':1, 'userId':0})
      .then((res)=>{
        expect(res.body).toEqual(expected)
        done()  
    })
  })
})
