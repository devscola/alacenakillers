const cors = require('cors')
const express = require('express')
const api = express()
const bodyparser = require('body-parser')
const Service = require('./Service')

const service = new Service()

api.use(express.json({ limit: '50mb'}))

api.use(bodyparser.text({ type: 'text/html' }))

api.use(cors())

api.get('/', (req, res) => {
  res.send('ola ke ase')
})

api.post('/create', (req, res) => {
  let productAndUser = req.body
  const result = service.createProduct(productAndUser)

  res.json(result)
})

api.get('/retrieveProducts', (req, res) => {
  const result = service.retrieveProducts()

  res.json(result)
})

api.post('/deleteProduct', (req, res) => {
  const productPositionAndUser = req.body
  const result = service.deleteProduct(productPositionAndUser)
  
  res.send(result)
})

api.get('/createUser', (req, res) => {
  const result = service.createUser()

  res.json(result)
})

api.post('/retrieveUserUniqueProduct', (req, res) => {
  const productPositionAndUser = req.body
  const result = service.retrieveUserUniqueProduct(productPositionAndUser)
  res.json(result)
})

api.post('/modifyProduct', (req, res) => {
  const result = service.modifyProduct(req.body)
  res.send(result)
})

module.exports = api