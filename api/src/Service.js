const Repository = require('./Repository')

class Service {
  constructor() {
    this.repository = new Repository()
  }

  createProduct(productAndUser) {
    const result = this.repository.create(productAndUser)
    
    return result
  }

  retrieveProducts() {
    const result = this.repository.retrieve()
    
    return result
  }

  deleteProduct(productPositionAndUser) {
    const result = this.repository.delete(productPositionAndUser)
    
    return result
  }
  createUser() {
    const result = this.repository.createUser()

    return result
  }
  retrieveUserUniqueProduct(productPositionAndUser) {
    const result = this.repository.retrieveUserUniqueProduct(productPositionAndUser)
    
    return result
  }

  modifyProduct(productIndexAndQuantity){
    const result = this.repository.modifyProduct(productIndexAndQuantity)

    return result
  }
}

module.exports = Service