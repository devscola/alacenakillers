class Repository {
  constructor() {
    this.usersLists = []
    this.userId = '0'
  }
  
  create(productAndUser) {
    this.userId = productAndUser.userId

    if (this.usersLists.length <= this.userId){
      this.usersLists.push([])
    }
    if (productAndUser.product != ""){
      this.usersLists[this.userId].push(productAndUser.product)
    }
  
    return productAndUser.product
  }

  retrieve() {
    const result = this.usersLists[this.userId]
    
    return result
  }

  delete(productPositionAndUser) {
    this.userId = productPositionAndUser.userId
    this.usersLists[this.userId].splice(productPositionAndUser.productPosition, 1)


    return this.usersLists[this.userId]
  }

  createUser() {
    this.userId = this.usersLists.length
  
    return this.userId
  }

  retrieveUserUniqueProduct(productPositionAndUser) {
    this.userId = productPositionAndUser.userId
    const quantity = this.usersLists[this.userId][productPositionAndUser.productPosition].match(/\S*$/g)
    let product = this.usersLists[this.userId][productPositionAndUser.productPosition].replace(quantity[0], '').trimEnd()
    let productAndQuantity = `${product},${quantity[0]}`

    return productAndQuantity
  }

  modifyProduct(productIndexAndQuantity){
    this.userId = productIndexAndQuantity.userId
    const product = productIndexAndQuantity.product
    const position = productIndexAndQuantity.position


    this.usersLists[this.userId].splice(position,1,product)
    
    return this.usersLists[this.userId]
  }
}

module.exports = Repository