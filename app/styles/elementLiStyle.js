const style = `
  <style>
    .elementLi {
    display: inline-block;
    }

    div {
    padding: 0px;
    border: thin;
    border-bottom-color: white;
    border-bottom-style: solid;
    }

    li {
    background-color: #7aabca;
    padding-top: 7px;
    padding-left: 40px;
    padding-right: 20px;
    list-style-type: none;
    }

    .deleteButton {
    display: flex;
    float: right;
    margin-right: 15px;
    display: inline;
    background-image: url(../../img/icons/alacena_delete_low.png);
    }
  </style>
`

export default style
