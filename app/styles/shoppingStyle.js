const style = `<style>

  .mainContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-top: 30px;
  }

  .listObject {
    display: flex;
    flex-direction: column;
    margin-top: 30px;
  }

  .listLabel {
      margin: auto;
      width: 60%;
      min-width: 277px;
      max-width: 360px;
  }

  #listContainer {
      display: block;
      display: flex;
      flex-direction: column;
      margin: auto;
      margin-top: 1px;
      width: 60%;
      min-width: 277px;
      max-width: 360px;
      height: 300px;
      background-color: #7aabca;
      border: none;
      overflow: auto;
  }

  .lines {
      display: flex;
      width: 100%;
  }

  .buttonDiv {
    display: flex;
    object-position: top;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    margin: auto;
    margin-top: 30px;
    padding: 5px;
    position: relative;
    display: block;
    height: 30px;
    width: 80%;
    min-width: 277px;
    max-width: 405px;
    background-color: var(--backgrounBlue);
  }

  h3 {
    text-align: center;
    margin: 1px;
  }
  
  </style>`

  export default style