const style = `
  <style>
    ::placeholder {
        color: #DDD;
        opacity: 0.7;
        font-weight: 400;
    }

    #labelSelector {
      font-weight: 100;
    }

    .input {
      width: 100%;
      height: 35px;
      background-color: #7aabca;
      border-top-left-radius: 8px;
      color: white;
      font-size: 18px;
      padding-left: 8px;
      font-weight: bold;
      text-align: left;
      border: none;
      box-shadow:inset 0 0 6px rgba(23, 46, 62, 0.6);
    }
    </style>

`

export default style
