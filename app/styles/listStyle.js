const style = `
  <style>
    ul {
      width: 92%;
      padding: 0px;
      margin: auto;
      margin-top: 20px;
    }

    .linesGroup {
      padding: 0px;
      margin: 0px;
      width: 100%;
    }

    hr {
      border: 0px;
      border-top: 1px solid white;
      margin-top: 32px;
      width: 92%;
    }
    p {
      font-size: 18px;
      padding-left: 8px;
      font-weight: 200;
      text-align: left;
    }

    #list {
      line-height: 1.5em;
      font-size: 1.2em;
      color: black;
      font-weight: 500;
      width: 100%;
      padding: 0px;
    }
  </style>
`

export default style
