const style = `
    <style>

    .alertBackground {
    position: fixed;
    width: 100%;
    height: 100vh;
    top: 0;
    left: 0;
    background-color: rgba(0,0,0,0.6);
    display: flex;
    justify-content: center;
    align-items: center;
    }

    .alert {
    background-color: #6498be;
    width: 19rem;
    height: 15rem;
    box-sizing: border-box;
    position: absolute;
    justify-content: center;
    text-align: center;
    border: 7px solid #7aabca ;
    
    }

    .title {
    font-size: 1.2rem;
    font-weight: bold;
    text-transform: uppercase;
    color: white;
    padding: 1rem;

    }

    .alertContent {
    font-size: 1.1rem;
    color: white;
    padding: 1rem;
    margin-top: 1.5rem;
    height: 3rem;

    }

    .buttonDiv {
    display: flex;
    justify-content: center;
    }

    .buttonConfirm {
    margin-right: 1rem;
    width: 35%;
    }

    .buttonCancel {
    width: 35%;
    }

    </style>

`

export default style