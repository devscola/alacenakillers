const style = `
  <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    #labelSelector {
      font-weight: 100;
    }

    .input {
      width: 100%;
      height: 35px;
      background-color: #7aabca;
      border-top-left-radius: 8px;
      color: white;
      font-size: 18px;
      padding-left: 8px;
      font-weight: bold;
      text-align: left;
      border: none;
      box-shadow:inset 0 0 6px rgba(23, 46, 62, 0.6);
      } 
    </style>
`

export default style