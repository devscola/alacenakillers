const style = `<style>

  div {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    box-sizing: border-box;
  }

  .mainContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-top: 30px;
  }

  .inputsDiv {
    position: relative;
    display: flex;
    justify-content: center;
    margin: auto;

    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    height: 45px;
    width: 80%;
    min-width: 280px;
    max-width: 405px;
    background-color: var(--backgrounBlue);
  }

  .buttonDiv {
    display: flex;
    object-position: top;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    margin: auto;
    margin-top: 5px;
    padding: 5px;
    position: relative;
    display: block;
    height: 40px;
    width: 80%;
    min-width: 277px;
    max-width: 405px;
    background-color: var(--backgrounBlue);
  }

  #product {
    justify-content: center;
    box-sizing: border-box;
    position: absolute;
    top: -13px;
    left: 5px;
    padding: 0px;
    margin: auto;
    width: 80%;
    min-width: 215px;
    max-width: 300px;
    height: 45px;
  }

  #quantity {
    box-sizing: border-box;
    position: absolute;
    top: -13px;
    right: 15px;
    padding: 0px;
    margin: auto;
    width: 20%;
    min-width: 45px;
    max-width: 80px;
    height: 45px;
    margin-bottom: 10px;
  }

  .listObject {
    display: flex;
    flex-direction: column;
    margin-top: 30px;
  }

  .listLabel {
      margin: auto;
      width: 60%;
      min-width: 277px;
      max-width: 360px;
  }

  #listContainer {
      display: block;
      display: flex;
      flex-direction: column;
      margin: auto;
      margin-top: 1px;
      width: 60%;
      min-width: 277px;
      max-width: 360px;
      height: 300px;
      background-color: #7aabca;
      border: none;
      overflow: auto;
  }

  .lines {
      display: flex;
      width: 100%;
  }

</style>`

export default style