module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
          browsers: [
            "last 2 versions", "safari >= 13",
            "last 2 versions", "firefox >= 76",
            "last 2 versions", "chrome >= 79",
            "last 2 versions","opera >= 68"
          ]
        },
      },
    ],
  ],
};