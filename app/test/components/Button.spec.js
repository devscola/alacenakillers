import button from '../../src/components/button.js'

describe('ProductButton',() => {
  it('dispatch an event',() => {
    const componentButton = document.createElement('component-button')
    button('component-button')
    document.querySelector('body').appendChild(componentButton)    

    const mock = jest.fn()
    componentButton.setAttribute('clickEventName','addProduct')
    document.addEventListener('addProduct', (event)=>{ mock(event) })
    const event = new Event('click', { bubbles: true })

    componentButton.shadowRoot.querySelector('button').dispatchEvent(event)

    expect(mock).toHaveBeenCalled()
  })

})
  