import list from '../../src/components/list.js'

describe('ProductList', () => {
  it('adds products to the list', () => {
    const componentList = document.createElement('component-list')
    list('component-list')
    document.querySelector('body').appendChild(componentList)
    
    const products = ['Patatas 1', 'Pipas 2', 'Pan 1']
    componentList.setAttribute('products', products)
    
    expect(componentList.shadowRoot.querySelector('ul').querySelectorAll('component-li').length).toEqual(3)
  })
})