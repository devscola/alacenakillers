import input from '../../src/components/textInput.js'

describe('ComponentInput',()=>{

  it('productInput dispatch an event',()=>{
    const productInput = document.createElement('component-input')
    input('component-input')
    document.querySelector('body').appendChild(productInput)
    
    const mock = jest.fn()
    document.addEventListener('product.name.changed', (event)=>{ mock(event) })
    const event = new Event('change', { bubbles: true })

    productInput.shadowRoot.querySelector('input').dispatchEvent(event)

    expect(mock).toHaveBeenCalled()
  })
  
})

  
