import shoppingView from '../components/shopping.js'

export default class Shopping {
  constructor(view_bus, service_bus){
    this.view_bus = view_bus
    this.service_bus = service_bus
    this.shopping_cart = []
    let element_name = 'shopping-component'
    shoppingView(element_name)
    
    this.element = document.getElementsByTagName(element_name)[0]

    this.registerEvents()

    this.service_bus.publish('retrieveProducts')
    this.service_bus.subscribe('productsRetrieved', this.loadProducts.bind(this))
  }

  registerEvents(){
    this.element.addEventListener('switch.view', () => {
      this.switchToShoppingView()
    })

    this.element.addEventListener('cart.product.added', (event) => {
      this.cartProductAdd(event) 
    })
  }

  cartProductAdd(product_id){
    if (this.shopping_cart.includes(product_id.detail)) {
      this.shopping_cart.splice((this.shopping_cart.indexOf(product_id.detail)), 1)
    } else {
      this.shopping_cart.push(product_id.detail)
    }
  }

  switchToShoppingView(){
    this.service_bus.publish('products.checked', this.shopping_cart)
    this.view_bus.publish('manager.switch')
  }

  loadProducts(listOfElements) {
    this.element.dispatchEvent(
      new CustomEvent('load.products', {
        composed: true,
        detail: listOfElements
      })
    )
  }
}