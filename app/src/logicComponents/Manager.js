import managerView from '../components/manager.js'

export default class Manager {
  constructor(view_bus, service_bus){
    this.product_name = ''
    this.product_quantity = '1'
    this.view_bus = view_bus
    this.service_bus = service_bus

    let element_name = 'manager-component'
    managerView(element_name)
    
    this.element = document.getElementsByTagName(element_name)[0]
  
    this.registerEvents()

    this.service_bus.publish('retrieveProducts')
    this.service_bus.subscribe('productsRetrieved', this.loadProducts.bind(this))
    this.service_bus.subscribe('productUserRetrieved', this.editProduct.bind(this))
  }

  registerEvents() {
    this.element.addEventListener('product.name.changed', (event) => {
      this.product_name = event.detail
    })

    this.element.addEventListener('quantity.product.changed', (event) => {
      this.product_quantity = event.detail
    })

    this.element.addEventListener('deleteLi',  (clickedDeleteEvent) => {
      this.service_bus.publish('deleteProduct', clickedDeleteEvent.detail)
    })

    this.element.addEventListener('switch.view', () => {
      this.view_bus.publish('shopping.switch')
    })

    this.element.addEventListener('enter.pressed', () => {
      this.addProduct()
    }) 

    this.element.addEventListener('add.product.clicked', () => {
      this.addProduct()
    })

    this.element.addEventListener('modifyLi', (clickedModifyEvent) => {
      this.modifyProductFromList(clickedModifyEvent)
    })
    
    this.element.addEventListener('save.modified.li', () => {
      this.updateProduct()
    })
  }

  loadProducts(listOfElements) {
    this.element.dispatchEvent(
      new CustomEvent('load.products', {
        composed: true,
        detail: listOfElements
      })
    )
  }

  editProduct(retrievedProductAndQuantity){
    this.element.dispatchEvent(
      new CustomEvent('edit.mode', {
        composed: true,
        detail: retrievedProductAndQuantity
      })
    )
  }

  addProduct() {
    if (!this.product_name) {
      this.dispatchAlert('nombre')
      this.element.dispatchEvent(
        new CustomEvent('product.name.empty', {
          bubbles: true,
          composed: true
        })
      )
      return
    }
    if (!this.product_quantity) {
        this.dispatchAlert('cantidad')
        this.element.dispatchEvent(
          new CustomEvent('quantity.name.empty', {
            bubbles: true,
            composed: true
          })
        )
        return
      }
    this.publishProduct(`${this.product_name} ${this.product_quantity}`)
  }
    
  dispatchAlert(name) {
    alert(`Campo Requerido Ausente. 
      No puedes añadir un producto sin ${name}`)
  }

  publishProduct(joinedProductAndQuantity) {
    this.service_bus.publish('createProduct', joinedProductAndQuantity)
    this._resetValues()
  }

  _resetValues() {
    this.product_quantity = '1'
    this.product_name = ''
  }

  modifyProductFromList(clickedModifyEvent){
    this.service_bus.publish('retrieveProductUser', clickedModifyEvent.detail)
    this.indexLi = clickedModifyEvent.detail
  }

  updateProduct() {
    this.service_bus.publish('retrieveModifyProduct',`${this.product_name} ${this.product_quantity}, ${this.indexLi}` )
    this.element.dispatchEvent(
      new CustomEvent('update.product', {
        composed: true
      })
    )
    this._resetValues()
  }
}