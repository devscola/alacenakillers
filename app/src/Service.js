import ApiClient from './infraestructure/ApiClient'

class Service {

  constructor(bus) {
    this.bus = bus
    this.subscriptions()
    this.managerUserId()
    this.userId = document.cookie.split('=')[1]
  }

  subscriptions() {
    this.bus.subscribe('createProduct', this.create.bind(this))
    this.bus.subscribe('deleteProduct', this.delete.bind(this))
    this.bus.subscribe('retrieveProductUser', this.retrieveProductUser.bind(this))
    this.bus.subscribe('retrieveModifyProduct', this.saveModifyLi.bind(this))
    this.bus.subscribe('retrieveProducts', this.retrieveProducts.bind(this))
    this.bus.subscribe('products.checked', this.deleteProductsChecked.bind(this))
  }

  async managerUserId(){
    const cookiesCaptured = document.cookie
    if (cookiesCaptured == '') {
      this.userId = await ApiClient.getJson('createUser')
      document.cookie = 'userId=' + this.userId + ';expires=' + new Date(9999, 0, 1).toUTCString()
    }
    this.userId = document.cookie.split('=')[1]
  }

  async create(joinedProductAndQuantity) {
    let product = ""
    if(joinedProductAndQuantity != undefined){
      product = joinedProductAndQuantity
    }

    await ApiClient.postJson('create', {'product':product, 'userId':this.userId})
  
    this.bus.publish('retrieveProducts')
  }
  
  async retrieveProducts(){
    const products = await ApiClient.getJson('retrieveProducts')
    this.bus.publish('productsRetrieved', products)
  }  
    
  async delete(productListPosition) {
    await ApiClient.postJson('deleteProduct', {'productPosition':productListPosition, 'userId':this.userId})
    this.bus.publish('retrieveProducts')
  }

  async retrieveProductUser(productIndex){
    const productAndQuantity = await ApiClient.postJson('retrieveUserUniqueProduct', {'productPosition':productIndex, 'userId':this.userId})

    this.bus.publish('productUserRetrieved', productAndQuantity)
  }

  async saveModifyLi(productIndexAndQuantity) {
    const [product, index] = productIndexAndQuantity.split(',')
    await ApiClient.postJson('modifyProduct', {'product':product, 'position':index, 'userId':this.userId})
    this.bus.publish('retrieveProducts')
  }

  async deleteProductsChecked(productsChecked){
    const productsCheckedLength = productsChecked.length
    for (let i = 1; i <= productsCheckedLength; i++){
      let index = Math.max(...productsChecked)
      await this.delete(index)
      productsChecked.splice((productsChecked.indexOf(index.toString())), 1)
    }
  }

}

export default Service