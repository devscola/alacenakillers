export default class Component extends HTMLElement {
    constructor(template, element_name) {
        super()
        this.root = this.attachShadow({ mode: 'open' })
        const template_element = document.createElement('template')
        template_element.innerHTML = template
        this.root.appendChild(template_element.content.cloneNode(true))

    }
    static define(element_name){
        window.customElements.get(element_name) || window.customElements.define(element_name, this)
    }
    
}
