import Component from '../infraestructure/Component'
import style from '../../styles/numberInputStyle'

let template = `
  
  ${style}

	<div>
		<label id="labelSelector"></label>
		<input id="inputSelector" type="number" class="input" max="99,9" min='1.0'></input>
	</div>
`

class NumberInput extends Component {
  constructor() {
    super(template)
    this.input = this.root.querySelector('input')
  }

  connectedCallback(){
    this.input.addEventListener('change', (inputChangeEvent) => {
      this.dispatchKeyupEvent(inputChangeEvent)
    })
    this.input.addEventListener('keyup', (inputKeyupEvent) => {
      if (inputKeyupEvent.keyCode == 13) {
        this.dispatchEnterPressedEvent()
      }

    })
  }

  static get observedAttributes() {
    return [
      'clear',
      'focus',
      'label',
      'quantity'
    ];
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.defaultValueChange(name, newVal)
    this.clearInputChange(name, newVal)
    this.focusChanged(name)
    this.labelChange(name, newVal)
  }

  defaultValueChange(attributeName, attributeContent) {
    const event = new Event('change')
    if (attributeName == 'quantity') {
      this.root.querySelector('#inputSelector').value = attributeContent
      this.input.dispatchEvent(event)
    }
  }

  clearInputChange(attributeName) {
    if (attributeName == 'clear') {
      this.input.value = '1'
    }
  }

  focusChanged(attributeName) {
    if (attributeName == 'focus') {
      this.root.querySelector('#inputSelector').focus()
    }
  }

  labelChange(attributeName, attributeContent) {
    if (attributeName == 'label') {
      this.root.querySelector('#labelSelector').innerHTML = attributeContent
    }
  }

  dispatchKeyupEvent(pressedKeyEvent) {
    const isInteger = pressedKeyEvent.target.value % 1
    if (isInteger != 0) {
      const regex = /.*\.\d/
      const singleFloat = pressedKeyEvent.target.value.match(regex)
      pressedKeyEvent.target.value = singleFloat[0]
    }

    this.dispatchEvent(
      new CustomEvent('quantity.product.changed', {
        bubbles: true,
        detail: this.input.value,
        composed: true
      })
    )
  }

  dispatchEnterPressedEvent() {
    this.dispatchEvent(
      new CustomEvent('enter.pressed', {
        bubbles: true,
        detail: 'true',
        composed: true
      })
    )
  }
}

export default function(element_name){
  NumberInput.define(element_name)
}