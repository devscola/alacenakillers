import Component from '../infraestructure/Component'
import style from '../../styles/elementLiStyle'

let template = `

  ${style}    

  <div>
    <li class='elementLi'></li>
    <component-button
      id='modifyButton'
      label='Modificar'
      clickEventName='modifyLi'
      buttonstyle=''>
    </component-button>
    <component-button
      class='deleteButton'
      label='Eliminar'
      clickEventName='deleteLi'
      buttonstyle=''>
    </component-button>
  </div>
`

class ElementLi extends Component {
  constructor () {
    super(template)
  }

  static get observedAttributes() {
    return ['changecontent', 'changebuttonid', 'connected']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.changeContent(name, newVal)
    this.changeButtonId(name, newVal)
    this.enable(name, newVal)
  }

  changeButtonId(attributeName, attributeContent) {
    if(attributeName == 'changebuttonid'){
      const everyButton = this.root.querySelectorAll('component-button')
      everyButton.forEach(element => {
        element.setAttribute(attributeName, attributeContent)
      })
    }
  }

  enable(attributeName, attributeContent) {
    if(attributeName == 'connected'){
      const everyButton = this.root.querySelectorAll('component-button')
      everyButton.forEach(element => {
        element.setAttribute('connected', attributeContent)
      })
    }
  }

  changeContent(attributeName, attributeContent){
    if (attributeName == 'changecontent') {
      this.root.querySelector('.elementLi').innerHTML = attributeContent
    }
  }
}

export default function(element_name){
  ElementLi.define(element_name)
}
