import Component from '../infraestructure/Component'
import style from '../../styles/textInputStyle'

let template = `
  
  ${style}

	<div>
		<label id="labelSelector"></label>
		<input id="inputSelector" class="input"></input>
	</div>
`

class TextInput extends Component {
  constructor() {
    super(template)
    this.input = this.root.querySelector('input')
  }

  connectedCallback(){
    this.input.addEventListener('change', (inputChangeEvent) => {
      this.dispatchKeyupEvent(inputChangeEvent)
    })
    this.input.addEventListener('keyup', (inputKeyupEvent) => {
      if (inputKeyupEvent.keyCode == 13) {
        this.dispatchEnterPressedEvent()
      }
    })
  }

  static get observedAttributes() {
    return [
      'clear',
      'focus',
      'placeholder',
      'label',
      'product'
    ]
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.clearInputChange(name, newVal)
    this.focusChanged(name)
    this.labelChange(name, newVal)
    this.defaultValueChange(name, newVal)
  }

  defaultValueChange(attributeName, attributeContent) {
    const event = new Event('change')
    if (attributeName == 'product') {
      this.root.querySelector('#inputSelector').value = attributeContent
      this.input.dispatchEvent(event)
    }
  }

  clearInputChange(attributeName) {
    if (attributeName == 'clear') {
      this.input.value = ''
    }
  }

  focusChanged(attributeName) {
    if (attributeName == 'focus') {
      this.root.querySelector('#inputSelector').focus()
    }
  }

  labelChange(attributeName, attributeContent) {
    if (attributeName == 'label') {
      this.root.querySelector('#labelSelector').innerHTML = attributeContent
    }
  }

  dispatchKeyupEvent() {
    this.dispatchEvent(
      new CustomEvent('product.name.changed', {
        bubbles: true,
        detail: this.input.value,
        composed: true
      })
    )
  }

  dispatchEnterPressedEvent() {
    this.dispatchEvent(
      new CustomEvent('enter.pressed', {
        bubbles: true,
        detail: 'true',
        composed: true
      })
    )
  }

}

export default function(element_name){
  TextInput.define(element_name)
}
