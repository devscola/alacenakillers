import Component from '../infraestructure/Component'

let template = `
  <style>
    .button {
      display: block;
      margin: 0px;
      width: 100%;
      height: 30px;
      background-color: #7aabca;
      border-bottom-left-radius: 8px;
      border-bottom-right-radius: 8px;
      color: white;
      font-size: 18px;
      font-weight: bold;
      text-align: center;
      border: none;
    }
  </style>
  <button></button>
`
class Button extends Component {
  constructor() {
    super(template)
    this.button = this.root.querySelector('button')
  }
  
  connectedCallback() {
    this.button.addEventListener('click', (event) => {
      this.addProduct(event)
    })
  }

  addProduct(event) {
    this.dispatchEvent(
      new CustomEvent(this.clickEventName, {
        bubbles: true,
        detail: event.target.id,
        composed: true,
      })
    )
  }

  static get observedAttributes() {
    return ['label', 'clickEventName', 'changebuttonid', 'classname', 'connected']
  }

  get clickEventName() {
    return this.getAttribute('clickEventName')
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.labelChange(name, newVal)
    this.addId(name, newVal)
    this.disconnectButton(name, newVal)
    this.setButtonStyle(name, newVal)
  }

  disconnectButton(attributeName, attributeContent) {
    if (attributeName == 'connected' && attributeContent !== 'true') {
      this.root.querySelector('button').disabled = true
    }
  }

  setButtonStyle(attributeName, attributeContent) {
    if (attributeName == 'classname') {
      this.root.querySelector('button').setAttribute('class', attributeContent)
    }
  }

  addId(attributeName, attributeContent) {
    if (attributeName == 'changebuttonid') {
      this.root.querySelector('button').setAttribute('id', attributeContent)
    }
  }

  labelChange(attributeName, attributeContent) {
    if (attributeName == 'label') {
      this.root.querySelector('button').innerHTML = attributeContent
    }
  }
}

export default function(element_name){
  Button.define(element_name)
}