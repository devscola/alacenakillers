import Component from '../infraestructure/Component'
import style from '../../styles/confirmationDialogStyle'

let template = `
  
  ${style}

  <div class='alertBackground'>
    <div class='alert'>
      <div class='title'>
       Confirmar iniciar compra
      </div>
      <div class='alertContent'>
        ¿Estas segura/o de que quieres iniciar la compra?
      </div>
      <div class='buttonDiv'>
        <div class='buttonConfirm'>
          <component-button 
            label="Confirmar"
            clickEventName="switch.view"
            classname='button'
            >
          </component-button>
        </div>
        <div class='buttonCancel'>
          <component-button 
            label="Cancelar"
            clickEventName="dialog.cancel"
            className='button'
            >
          </component-button>
        </div>
      </div>
    </div>
  </div>
`

class ConfirmationDialog extends Component {
  constructor() {
    super(template)
  }

  static get observedAttributes() {
    return ['title', 'contenttext']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this.changeTitle(name, newVal)
    this.changeContent(name, newVal)
  }

  changeTitle(attributeName, attributeContent){
    if(attributeName === 'title') {
      this.root.querySelector('.title').innerHTML = attributeContent
    }
  }

  changeContent(attributeName, attributeContent){
    if(attributeName === 'contenttext') {
      this.root.querySelector('.alertContent').innerHTML = attributeContent
    }
  }
}

export default function(element_name){
  ConfirmationDialog.define(element_name, this)
}
