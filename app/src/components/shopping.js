import Component from '../infraestructure/Component'
import shoppingList from './shoppingList'
import style from '../../styles/shoppingStyle'

let template = `
  
  ${style}

  <div class='mainContainer'>
    <h3>Realizando la compra</h3>
    <div class='listObject'>
      <p class='listLabel'> Lista de la compra: </p>
        <div id='listContainer'>
            <shopping-list id='list'></shopping-list>
        </div>
    </div>
      <div class='buttonDiv'>
          <component-button 
            id="shoppingModeButton"
            label="Finalizar compra"
            clickEventName="shopping.confirmation.alert"
            classname='button'
            >
          </component-button>
      </div>
  </div>
`
class ShoppingView extends Component {
  constructor() {
    super(template)

    this.button = this.root.querySelector('component-button')
    this.list = this.root.querySelector('shopping-list')


    this.addEventListener('load.products', () => {
      this.list.setAttribute('products', event.detail)
    })

    shoppingList('shopping-list')


    this.addEventListener('shopping.confirmation.alert', () => {
      let position = this.root.querySelector('.buttonDiv')
      position.insertAdjacentHTML('afterend', '<component-alert></component-alert>')
      
      let alert = this.root.querySelector('component-alert')
      alert.setAttribute('title', 'Confirmar finalizar compra')
      alert.setAttribute('contenttext', '¿Estas segura/o de que quieres finalizar la compra?')
    })

    this.addEventListener('dialog.cancel', () => {
      let alert = this.root.querySelector('component-alert')
      if (alert != null){
        alert.remove()
      }
    })
  }

}

export default function(element_name){
  ShoppingView.define(element_name)
}
