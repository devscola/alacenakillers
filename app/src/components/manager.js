import Component from '../infraestructure/Component'
import textInput from './textInput'
import button from './button'
import numberInput from './numberInput'
import list from './list'
import confirmationDialog from './confirmationDialog'
import style from '../../styles/mangerStyle'

let template = `

  ${style}

  <div class='mainContainer'>
    <div class='inputsDiv'>
      <text-input
        id='product'
        label='Añadir elementos a la lista:'
        placeholder='ejem:cacahuetes 1kg'
        >
      </text-input>
      <number-input
        id='quantity'
        label='Uds/Kgs:'
        quantity='1'
        >
      </number-input>
    </div>
    <div class='buttonDiv'>
      <component-button
          id='addButton'
          label='Añadir'
          clickEventName='add.product.clicked'
          classname='button'>
      </component-button>
    </div>

    <div class='listObject'>
        <p class='listLabel'> Lista de la compra: </p>
        <div id='listContainer'>
            <component-list id='list'></component-list>
        </div>
        <div class='mainContainer'>
          <div class='buttonDiv'>
              <component-button 
              id="shoppingModeButton"
              label="Iniciar compra"
              clickEventName="shopping.confirmation.show"
              classname='button'
              >
              </component-button>
          </div>
        </div>
    </div>
  </div>
  
`
class ManagerView extends Component {
  constructor() {
    super(template)
    this.button = this.root.querySelector('button')
    this.textInput = this.root.querySelector('text-input')
    this.numberInput = this.root.querySelector('number-input')
    this.list = this.root.querySelector('component-list')
    this.addButton = this.root.getElementById('addButton')

    this.registerEvents()

    textInput('text-input')
    numberInput('number-input')
    button('component-button')
    list('component-list')
  }

  registerEvents(){
    this.addEventListener('product.name.empty', () => {
      this.textInput.setAttribute('focus', 'true')
    })

    this.addEventListener('quantity.name.empty', () => {
      this.numberInput.setAttribute('focus', 'true')
    })

    this.addEventListener('load.products', (event) => {
      this.loadProducts(event)
    })

    this.addEventListener('edit.mode', (event) => {
      this.editMode(event)
    })

    this.addEventListener('update.product', () => {
      this.updateProduct()
    })

    this.addEventListener('shopping.confirmation.show', () => {
      this.showShoppingConfirmation()
    }) 

    this.addEventListener('dialog.cancel', () => {
      this.cancelDialog()
    })
  }

  loadProducts(event) {
    this.list.setAttribute('products', event.detail)
    this.textInput.setAttribute('clear', 'productCleared')
    this.numberInput.setAttribute('clear', 'quantityCleared')
  }

  editMode(event) {
    const [product, quantity] = event.detail.split(',')

    this.textInput.setAttribute('product', product)
    this.numberInput.setAttribute('quantity', quantity)
    this.addButton.setAttribute('label', 'Guardar')
    this.addButton.setAttribute('clickEventName', 'save.modified.li') 
    this.list.setAttribute('connected', true)
  }

  updateProduct() {
    this.addButton.setAttribute('label', 'Añadir')
    this.addButton.setAttribute('clickEventName', 'add.product.clicked')
    this.list.setAttribute('connected', false)
  }

  showShoppingConfirmation() {
    let position = this.root.querySelector('.mainContainer')
    position.insertAdjacentHTML('afterend', '<component-alert></component-alert>')
    confirmationDialog('component-alert')
  }

  cancelDialog() {
    let alert = this.root.querySelector('component-alert')
    alert.remove()
  }
}

export default function(element_name){
  ManagerView.define(element_name, this)
}
