import Component from '../infraestructure/Component'
import elementLi from './elementLi'
import style from '../../styles/listStyle'

let template = `

  ${style}

  <div>
    <ul></ul>
  </div>

  <div class='linesGroup'>
    <hr><hr><hr><hr><hr><hr><hr><hr><hr><hr>
  </div>
`
class List extends Component {
  constructor() {
    super(template)
    this.list = this.root.querySelector('ul')
    elementLi('component-li')
    this.isEnabled = true
    this.render()
  }

  static get observedAttributes() {
    return ['products', 'connected']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this[name] = newVal
    this.connectToggle(name)
    this.render()
  }

  connectToggle(attributeName){
    this.isEnabled = true
    if (attributeName == 'connected' && this.isEnabled) {
      this.isEnabled = false
    }
  }

  deleteRenderedLi(){
    while (this.list.hasChildNodes()){
      this.list.removeChild(this.list.firstChild)
    }
  }

  render() {
    this.deleteRenderedLi()
    const products = this.getAttribute('products')
    if (products){
      const splittedProducts = products.split(',')
      for (let product of splittedProducts){
        let listLi = document.createElement('component-li')
        this.list.appendChild(listLi)
        listLi.setAttribute('changecontent', product)
        listLi.setAttribute('changebuttonid', splittedProducts.indexOf(product))
        listLi.setAttribute('connected', this.isEnabled)
      }
    }
  }
}

export default function(element_name){
  List.define(element_name)
}