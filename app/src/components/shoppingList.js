import Component from '../infraestructure/Component'
import style from '../../styles/shoppingListStyle'

let template = `

  ${style}

  <div>
    <ul></ul>
  </div>
`
class ShoppingList extends Component {
  constructor() {
    super(template)
    this.list = this.root.querySelector('ul')
    this.render()
  }

  static get observedAttributes() {
    return ['products']
  }

  attributeChangedCallback(name, _oldVal, newVal) {
    this[name] = newVal
    this.render()
  }

  deleteRenderedLi(){
    while (this.list.hasChildNodes()){
      this.list.removeChild(this.list.firstChild)
    }
  }

  render() {
    this.deleteRenderedLi()
    const products = this.getAttribute('products')
    if (products){
      const splittedProducts = products.split(',')
      for (let product of splittedProducts){
        let id = splittedProducts.indexOf(product)
        this.createLi(product, id)
      }
    }
  }

  createLi(product, id){
    let list_li = document.createElement('li')

    let check_input = document.createElement('input')
    check_input.type = 'checkbox'
    check_input.value = id
    check_input.addEventListener('click', (event) => {
      this.dispatchEvent(
        new CustomEvent('cart.product.added', {
          detail: event.target.value,
          composed: true
        })
      )
      span.classList.toggle('disable')
    })

    let span = document.createElement('span')
    span.innerHTML = product

    let hr = document.createElement('hr')

    list_li.appendChild(check_input) 
    list_li.appendChild(span)
    list_li.appendChild(hr) 
    this.list.appendChild(list_li)
  }
}

export default function(element_name){
  ShoppingList.define(element_name)
}
