import Manager from './logicComponents/Manager'
import Shopping from './logicComponents/Shopping'
import Service from './Service'
import Bus from './infraestructure/Bus'

export default class App {
  constructor() {
    this.view_bus = new Bus()
    this.service_bus = new Bus ()
    new Service (this.service_bus)
    this.switchView('manager-component', 'shopping-component', Manager)

    this.view_bus.subscribe('shopping.switch', () => {
      this.switchView('shopping-component', 'manager-component', Shopping )
    })

    this.view_bus.subscribe('manager.switch', () => {
      this.switchView('manager-component', 'shopping-component', Manager)
    })
  }

  switchView(new_view, old_view, new_instance){
    let old_element = document.getElementsByTagName(old_view)[0]
    if(old_element){
      old_element.remove()
    }
    
    let new_element = document.createElement(new_view)
    document.getElementById('main').appendChild(new_element)

    new new_instance(this.view_bus, this.service_bus)
  }
}